jQuery.noConflict();
jQuery(document).ready(function($){

	$('html').addClass('js');

	// cycle
	$('.mod_custom-slideshow').each(function(){
		var i = $('.item',this).length;
		if(i>1){
			$(this).append('<a class="prev"/><a class="next"/>');
			$('.items',this).cycle({
				slides: '.item',
				speed: 600,
				timeout: 5000,
				pauseOnHover: true,
				prev: $('.prev',this),
				next: $('.next',this),
			});
		}
	});

	$('.panes').each(function(){
		$(this).cycle({
			slides: '.pane',
			speed: 1,
			timeout: 0,
			pager: $('.tabs',this),
			pagerTemplate: '',
		});
	});

	// iCheck
	$('input[type="checkbox"],input[type="radio"]').iCheck();

	// selectBoxIt
	$('select:not([multiple]').selectBoxIt();

	// file input style
	$('input[type=file]').each(function(){
		var uploadText = $(this).data('value');
		if (uploadText) {
			var uploadText = uploadText;
		} else {
			var uploadText = 'Choose file';
		}
		var inputClass=$(this).attr('class');
		$(this).wrap('<span class="fileinputs"></span>');
		$(this)
			.parents('.fileinputs')
			.append($('<span class="fakefile"/>')
			.append($('<input class="input-file-text" type="text" />')
			.attr({'id':$(this)
			.attr('id')+'__fake','class':$(this).attr('class')+' input-file-text'}))
			.append('<input type="button" value="'+uploadText+'">'));
		$(this)
			.addClass('type-file')
			.css('opacity',0);
		$(this)
			.bind('change mouseout',function(){$('#'+$(this).attr('id')+'__fake')
			.val($(this).val().replace(/^.+\\([^\\]*)$/,'$1'))
		})
	});

	// placeholder
	if (document.createElement('input').placeholder==undefined){
		$('[placeholder]').focus(function(){
			var input=$(this);
			if(input.val()==input.attr('placeholder')){
				input.val('');
				input.removeClass('placeholder')
			}
		}).blur(function(){
			var input=$(this);
			if(input.val()==''||input.val()==input.attr('placeholder')){
				input.addClass('placeholder');
				input.val(input.attr('placeholder'))
			}
		}).blur()
	}

	// go top
	var go_top = function(){
		var offset = $(window).scrollTop();
		if(offset>100){
			$('#gotop').addClass('on');
		} else {
			$('#gotop').removeClass('on');
		}
	}
	go_top();
	$(window).scroll(function(){
		go_top();
	});
	$('#gotop a').click(function(){ 
		$('html,body').animate({scrollTop:0},600);
		return false;
	});

});